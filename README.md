# 8769_forwarder

This POC script demonstrates capturing and replaying requests to TCP port 8769 over a SOCKS proxy.

Further detail will be published as a [Red Team Tech Note](https://gitlab-com.gitlab.io/gl-security/security-tech-notes/red-team-tech-notes/) once it is complete.

